const http = require("http");
const fs = require("fs");
const ftp = require("basic-ftp");
const parser = require("fast-xml-parser");
const compress = require("compression");

// Create a server object
http
  .createServer(function (req, res) {

    compress()(req,res,function(){})

    var url = req.url;

    if (url === "/parse-xml") {

      //HTML header
      res.writeHead(200, { "Content-Type": "text/html" });
      //Navigation
      res.write("<h1>Charles Faye Website API</h1>");
      res.write("<h3>Download XML and convert to JSON</h3>");
      res.write('<a href="/">Home Page</a>');
      res.write(" | ");
      res.write('<a href="/json">View JSON</a>');
      res.write(" | ");
      res.write('<a href="/xml">View XML</a>');

      async function xml2json() {
        //Connect to FTP and pull down XML
        const client = new ftp.Client();
        client.ftp.verbose = true;
        try {
          await client.access({
            host: "ftp.expertagent.co.uk",
            user: "Charles Faye",
            password: "EmxYRoel",
            secure: true,
          });
          console.log(await client.list())
          
          await client.downloadTo("xml/properties.xml", "properties.xml");
        } catch (err) {
          console.log(err);
        }
        client.close();

        //Parse to JSON options
        var options = {
          attributeNamePrefix: "@_",
          attrNodeName: "attr", //default is 'false'
          textNodeName: "#text",
          ignoreAttributes: false,
          ignoreNameSpace: false,
          allowBooleanAttributes: false,
          parseNodeValue: true,
          parseAttributeValue: false,
          trimValues: true,
          cdataTagName: "__cdata", //default is 'false'
          cdataPositionChar: "\\c",
          parseTrueNumberOnly: false,
          arrayMode: false, //"strict"
          stopNodes: ["parse-me-as-string"],
        };

        //Read XML and parse to JSON
        var xml = fs.readFileSync("xml/properties.xml", "utf-8");
        var jsonObj = parser.parse(xml, options);
        var jsonContent = JSON.stringify(jsonObj);

        console.log(jsonContent);

        //Write JSON to file
        fs.writeFile("json/properties.json", jsonContent, "utf8", function (
          err
        ) {
          if (err) {
            console.log("An error occured while writing JSON to file.");
            return console.log(err);
          }
          console.log("JSON file has been saved.");
        });
      }

      xml2json();

      res.end();
    } else if (url === "/json") {
      res.setHeader("Access-Control-Allow-Origin", "*");
      res.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
      //HTML header
      res.writeHead(200, { "Content-Type": "application/json" });
      //File output
      res.write(fs.readFileSync("json/properties.json", "utf-8"));
      res.end();
    } else if (url === "/xml") {
      //HTML header
      res.writeHead(200, { "Content-Type": "application/xml" });
      //File output
      res.write(fs.readFileSync("xml/properties.xml", "utf-8"));
      res.end();
    } else {
      //HTML header
      res.writeHead(200, { "Content-Type": "text/html" });
      //Navigation
      res.write("<h1>Charles Faye Website API</h1>");
      res.write("<h3>Home Page</h3>");
      res.write('<a href="/parse-xml">Download XML and convert to JSON</a>');
      res.write(" | ");
      res.write('<a href="/json">View JSON</a>');
      res.write(" | ");
      res.write('<a href="/xml">View XML</a>');
      res.end();
    }
  })
  .listen(8080, function () {
    console.log("Server started on port 8080");
  });